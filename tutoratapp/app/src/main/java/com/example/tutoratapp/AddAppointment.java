package com.example.tutoratapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class AddAppointment extends AppCompatActivity {

    EditText edtIDTutor;
    EditText edtIDCourse;
    EditText edtDuration;
    EditText edtDay;

    DatabaseManager myDb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);

        edtIDTutor = findViewById(R.id.edtIDTutor);
        edtIDCourse = findViewById(R.id.edtIDCourse);
        edtDuration = findViewById(R.id.edtDuration);
        edtDay = findViewById(R.id.edtDay);

        myDb = new DatabaseManager(this);
    }


    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void btnAddApointment(View view){
        Boolean appointmentAdded = myDb.makeAnAppointment(Integer.parseInt(edtIDTutor.getText().toString()), Integer.parseInt(edtIDCourse.getText().toString()), edtDay.getText().toString(), Integer.parseInt(edtDuration.getText().toString()));
        if(appointmentAdded){
            Toast.makeText(AddAppointment.this, "Rendez-vous enregistré!", Toast.LENGTH_SHORT).show();
            CancelingInputs(edtIDTutor, edtIDCourse, edtDuration, edtDay);
        }
        else{
            Toast.makeText(AddAppointment.this, "L'ID de l'étudiant ou l'ID du cours n'existe pas!", Toast.LENGTH_SHORT).show();
        }

    }

    public void btnCancelAdding(View view){
        CancelingInputs(edtIDTutor, edtIDCourse, edtDuration, edtDay);
    }

    public void CancelingInputs(EditText field1, EditText field2, EditText field3, EditText field4){
        field1.getText().clear();
        field2.getText().clear();
        field3.getText().clear();
        field4.getText().clear();
    }

    public void btnConsultListAppoint(View view){
        Intent i = new Intent(this, ListAppointmentsActivity.class);
        startActivity(i);
    }
}
