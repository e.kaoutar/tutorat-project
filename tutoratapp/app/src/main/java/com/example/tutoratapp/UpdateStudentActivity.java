package com.example.tutoratapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class UpdateStudentActivity extends AppCompatActivity {

    DatabaseManager myDb;
    EditText edtID;
    EditText edtNameEdit;
    EditText edtName2Edit;
    EditText edtEmailEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_student);

        myDb = new DatabaseManager(this);
        edtID = findViewById(R.id.edtIDEdit);
        edtNameEdit = findViewById(R.id.edtNameEdit);
        edtName2Edit = findViewById(R.id.edtName2Edit);
        edtEmailEdit = findViewById(R.id.edtEmailEdit);

    }


    public void btnEditStuddent(View view){
        Boolean isUpdated = myDb.UpdateStudent(Integer.parseInt(edtID.getText().toString()), edtNameEdit.getText().toString(), edtName2Edit.getText().toString(), edtEmailEdit.getText().toString());
        if(isUpdated){
            Toast.makeText(UpdateStudentActivity.this, "Data Updated", Toast.LENGTH_SHORT).show();
            CancelingInputs(edtID, edtNameEdit, edtName2Edit, edtEmailEdit);
        }
        else{
            Toast.makeText(UpdateStudentActivity.this, "Error in the process", Toast.LENGTH_SHORT).show();
        }
    }

    public void btnCancelEditing(View view){
        CancelingInputs(edtID, edtNameEdit, edtName2Edit, edtEmailEdit);
    }

    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }


    public void CancelingInputs(EditText field1, EditText field2, EditText field3, EditText field4){
        field1.getText().clear();
        field2.getText().clear();
        field3.getText().clear();
        field4.getText().clear();
    }
}
