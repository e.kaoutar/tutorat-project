package com.example.tutoratapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.icu.lang.UCharacterEnums;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

public class InfosStudentActivity extends AppCompatActivity {

    EditText edtNameSearch;
    TextView recordsInfos;
    TextView titledisplay;
    DatabaseManager myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infos_student);

        myDb = new DatabaseManager(this);
        edtNameSearch = findViewById(R.id.edtNameSearch);
        recordsInfos = findViewById(R.id.studentInfos);
        titledisplay = findViewById(R.id.TitleStudent);

    }

    public void btnInfosStud(View view){

        Cursor res = myDb.getStudentData(edtNameSearch.getText().toString());
        if(res.getCount() == 0){
            showMessage("Attention", "Aucun étudiant n'est trouvé pour "+ edtNameSearch.getText().toString());
            return;
        }
        else{
            titledisplay.setText("Enregistrements trouvés");
            StringBuffer buffer = new StringBuffer();
            while (res.moveToNext()){
                buffer.append("ID : "+ res.getString(0)+" \n");
                buffer.append("Nom : "+ res.getString(1)+" \n");
                buffer.append("Prénom : "+ res.getString(2)+" \n");
                buffer.append("Courriel : "+ res.getString(3)+" \n\n");
            }

            recordsInfos.setText(buffer.toString());
        }


    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
