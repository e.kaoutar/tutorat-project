package com.example.tutoratapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListCoursesActivity extends AppCompatActivity {

    DatabaseManager dbm;
    ListView listCourses;
    ArrayList<String> courses = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_courses);

        dbm = new DatabaseManager(this);
        listCourses = findViewById(R.id.listCourses);

        //Adapter
        Cursor res = dbm.showAllCourses();

        if(res.getCount() == 0){
            showMessage("Attention", "Aucun cours n'est admissible au tutorat maintenant");
            return;
        }
        else{

            StringBuffer buffer;
            while (res.moveToNext()){

                buffer = new StringBuffer();
                buffer.append("ID : "+ res.getString(0)+" \n");
                buffer.append("Titre : "+ res.getString(1)+" \n");
                buffer.append("Nombre d'heures : "+ res.getInt(2)+" \n");
                buffer.append("Programme : "+ res.getString(3)+" \n");

                courses.add(buffer.toString());

            }

            //calling adapter:
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, courses);
            listCourses.setAdapter(arrayAdapter);
        }

    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
