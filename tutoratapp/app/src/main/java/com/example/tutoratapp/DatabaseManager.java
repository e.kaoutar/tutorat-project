package com.example.tutoratapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseManager extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Tutoratteccart.db";
    public static final String TABLE_NAME_STUDENT = "Student";
    public static final String COL_1_1 = "ID";
    public static final String COL_2_1 = "Name";
    public static final String COL_3_1 = "Name2";
    public static final String COL_4_1 = "Email";

    //Second table
    public static final String TABLE_NAME_COURSE = "Course";
    public static final String COL_1_2 = "ID";
    public static final String COL_2_2 = "Title";
    public static final String COL_3_2 = "NbHours";
    public static final String COL_4_2 = "Program";

    //Third table
    public static final String TABLE_NAME_APPOINTMENT = "Appointment";
    public static final String COL_1_3 = "ID";
    public static final String COL_2_3 = "ID_Course";
    public static final String COL_3_3 = "ID_Student";
    public static final String COL_4_3 = "Day";
    public static final String COL_5_3 = "Duration";


    public DatabaseManager(Context context) {
        super(context, DATABASE_NAME, null  , 1);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_NAME_STUDENT+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Name2 TEXT, Email TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_NAME_COURSE+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, Title TEXT, NbHours INTEGER, Program TEXT)");
        sqLiteDatabase.execSQL("CREATE TABLE "+TABLE_NAME_APPOINTMENT+" (ID INTEGER PRIMARY KEY AUTOINCREMENT, ID_Course INTEGER, ID_Student INTEGER, Day TEXT, Duration INTEGER, FOREIGN KEY (ID_Course) REFERENCES Course(ID), FOREIGN KEY (ID_Student) REFERENCES Student(ID))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME_STUDENT);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME_COURSE);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME_APPOINTMENT);
        onCreate(sqLiteDatabase);
    }

    //insert a single student
    public boolean insertData(String Name, String Name2, String Email){

        SQLiteDatabase sqld = this.getWritableDatabase();
        ContentValues ctv = new ContentValues();
        ctv.put(COL_2_1, Name);
        ctv.put(COL_3_1, Name2);
        ctv.put(COL_4_1, Email);

        long result = sqld.insert(TABLE_NAME_STUDENT, null, ctv);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    //Get data of students who have the same keyword
    public Cursor getStudentData(String keyword){
        SQLiteDatabase sqld = this.getWritableDatabase();
        Cursor res = sqld.rawQuery("SELECT * FROM "+ TABLE_NAME_STUDENT + " WHERE "+ COL_2_1 + " LIKE '"+ keyword + "%'", null);
        return res;
    }

    //Update a student
    public boolean UpdateStudent(Integer ID, String Name, String Name2, String Email){
        SQLiteDatabase sqld = this.getWritableDatabase();
        ContentValues ctv = new ContentValues();
        ctv.put(COL_1_1, ID);
        ctv.put(COL_2_1, Name);
        ctv.put(COL_3_1, Name2);
        ctv.put(COL_4_1, Email);

        //whereArgs accepts only String[] type
        sqld.update(TABLE_NAME_STUDENT, ctv, "ID = "+ID, null);

        return true;
    }

    //Update a student
    public boolean UpdateCourse(){
        SQLiteDatabase sqld = this.getWritableDatabase();
        ContentValues ctv = new ContentValues();
        ctv.put(COL_1_2, 4);
        ctv.put(COL_2_2, "C#");
        ctv.put(COL_3_2, 137);
        ctv.put(COL_4_2, "Informatique de gestion");

        //whereArgs accepts only String[] type
        sqld.update(TABLE_NAME_COURSE, ctv, "ID = "+4, null);

        return true;
    }


    //Manual insertion of courses
    public boolean insertDataCourse(){
        SQLiteDatabase sqld = this.getWritableDatabase();
        ContentValues ctv = new ContentValues();
        ctv.put(COL_2_2, "Mariadb database");
        ctv.put(COL_3_2, 130);
        ctv.put(COL_4_2, "Gestion de raiseau");

        long result = sqld.insert(TABLE_NAME_COURSE, null, ctv);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    //Insert a new appointment
    public boolean makeAnAppointment(Integer ID_Course, Integer ID_Student, String Day, Integer Duration){

        SQLiteDatabase sqld = this.getWritableDatabase();
        ContentValues ctv = new ContentValues();
        ctv.put(COL_2_3, ID_Course);
        ctv.put(COL_3_3, ID_Student);
        ctv.put(COL_4_3, Day);
        ctv.put(COL_5_3, Duration);

        long result = sqld.insert(TABLE_NAME_APPOINTMENT, null, ctv);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }
    }

    //Show recent appointments
    public Cursor findAppointments(Integer ID_Course){
        SQLiteDatabase sqld = this.getWritableDatabase();
        Cursor res = sqld.rawQuery("SELECT * FROM "+ TABLE_NAME_APPOINTMENT + " WHERE "+ COL_2_3 + " = "+ ID_Course + "", null);
        return res;
    }

    //Show All courses
    public Cursor showAllCourses(){
        SQLiteDatabase sqld = this.getWritableDatabase();
        Cursor res = sqld.rawQuery("SELECT * FROM "+ TABLE_NAME_COURSE, null);
        return res;
    }

    //Show All appointments
    public Cursor showAllAppointments(){
        SQLiteDatabase sqld = this.getWritableDatabase();
        Cursor res = sqld.rawQuery("SELECT * FROM "+ TABLE_NAME_APPOINTMENT, null);
        return res;
    }

    //identify student
    public Cursor showStudentById(Integer ID_Student){
        SQLiteDatabase sqld = this.getWritableDatabase();
        Cursor res = sqld.rawQuery("SELECT * FROM "+ TABLE_NAME_STUDENT + " WHERE ID="+ ID_Student +"", null);
        return res;
    }


}
