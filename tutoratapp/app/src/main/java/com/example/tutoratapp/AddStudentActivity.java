package com.example.tutoratapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class AddStudentActivity extends AppCompatActivity{

    DatabaseManager myDb;
    EditText edtName, edtName2, edtEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        myDb = new DatabaseManager(this);
        edtName = findViewById(R.id.edtName);
        edtName2 = findViewById(R.id.edtName2);
        edtEmail = findViewById(R.id.edtEmail);



    }

    public void btnAddData(View view){
        boolean inserted = myDb.insertData(edtName.getText().toString(), edtName2.getText().toString(), edtEmail.getText().toString());
        if(TextUtils.isEmpty(edtName.getText().toString())) {
            edtName.setError("Entrée vide");
            return;
        }
        if(TextUtils.isEmpty(edtName2.getText().toString())) {
            edtName2.setError("Entrée vide");
            return;
        }
        if(TextUtils.isEmpty(edtEmail.getText().toString())) {
            edtEmail.setError("Entrée vide");
            return;
        }

        if(edtName.getText().toString().trim().length()>0 && edtName2.getText().toString().trim().length()>0 && edtEmail.getText().toString().trim().length()>0){
            if(inserted){
                Toast.makeText(AddStudentActivity.this, "L'ajout de l'étudiant est fait!", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(AddStudentActivity.this, "L'ajout a échoué, Réessayez!", Toast.LENGTH_SHORT).show();
            }
        }


    }

    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    public void btnCancelAdding(View view){
        CancelingInputs(edtName, edtName2, edtEmail);
    }

    public void CancelingInputs(EditText field1, EditText field2, EditText field3){
        field1.getText().clear();
        field2.getText().clear();
        field3.getText().clear();
    }



}
