package com.example.tutoratapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ListAppointmentsActivity extends AppCompatActivity {

    DatabaseManager dbm;
    ListView listAppointments;
    ArrayList<String> appointments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_appointments);

        dbm = new DatabaseManager(this);
        listAppointments = findViewById(R.id.listAppointements);

        Cursor res = dbm.showAllAppointments();

        if(res.getCount() == 0){
            showMessage("Attention", "Aucun rendez-vous n'est encore était pris!");
            return;
        }
        else{

            StringBuffer buffer;
            while (res.moveToNext()){

                //Cursor stud = dbm.showStudentById(Integer.parseInt(res.getString(0).toString()));
                buffer = new StringBuffer();
                buffer.append("L'étudiant numéro : "+ res.getString(0)+"\n");
                buffer.append("Cours numéro : "+ res.getString(1)+" \n");
                buffer.append("Plage d'horaire : "+ res.getInt(2)+" heures \n");
                buffer.append("Jour(s) : "+ res.getString(3)+" \n");

                appointments.add(buffer.toString());

            }

            //calling adapter:
            ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, appointments);
            listAppointments.setAdapter(arrayAdapter);
        }
    }

    public void showMessage(String title, String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }

    public void btnBackMenu(View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }
}
