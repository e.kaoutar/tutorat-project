package com.example.tutoratapp;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseManager myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
/*
        myDb = new DatabaseManager(this);
        if(myDb.insertDataCourse()){
            Toast.makeText(this, "Cours ajouté !", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Cours echoué !", Toast.LENGTH_SHORT).show();
        }
*/

        //myDb = new DatabaseManager(this);
        //myDb.UpdateCourse();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_addStudent) {
            //Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, AddStudentActivity.class);
            startActivity(i);
            return true;
        }

        if (id == R.id.action_showStudent) {
            //Toast.makeText(this, "Settings", Toast.LENGTH_SHORT).show();
            Intent i = new Intent(this, InfosStudentActivity.class);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_editStudent){
            Intent i = new Intent(this, UpdateStudentActivity.class);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_getCourses){
            Intent i = new Intent(this, ListCoursesActivity.class);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_setAppointment){
            Intent i = new Intent(this, AddAppointment.class);
            startActivity(i);
            return true;
        }

        if(id == R.id.action_listStudents){
            Intent i = new Intent(this, ListAppointmentsActivity.class);
            startActivity(i);
            return true;
        }



        return super.onOptionsItemSelected(item);
    }
}
